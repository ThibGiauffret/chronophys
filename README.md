# ChronoPhys

## Description

ChronoPhys est un logiciel libre pour réaliser des chronophotographies en Sciences-Physiques, développé par [Thibault Giauffret](https://ensciences.fr/).

Il se base sur les bibliothèques :
- [PyQt6](https://www.riverbankcomputing.com/software/pyqt/intro) pour l'interface graphique ;
- [OpenCV](https://opencv.org/) pour le traitement et la capture vidéo ;
- [Matplotlib](https://matplotlib.org/) pour l'affichage des graphiques.

## Installation

Pour installer ChronoPhys, il faut clone le dépôt git et installer les dépendances avec pip :

```bash
git clone https://framagit.org/ThibGiauffret/chronophys.git
cd chronophys
pip install pyperclip
pip install numpy
pip install matplotlib
pip install PyQt6
pip install flask
pip install qrcode
pip install opencv-python
pip install qtawesome
```
## Utilisation

Un tutoriel de prise en main est disponible [ici](https://www.ensciences.fr/read.php?article=820).

## License

Le code source de ChronoPhys est sous licence [GPLv3](https://www.gnu.org/licenses/gpl-3.0.html).
