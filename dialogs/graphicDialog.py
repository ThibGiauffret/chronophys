# Importation des librairies requises pour ce module
from PyQt6.QtWidgets import QDialog, QSizePolicy, QVBoxLayout, QWidget,QFileDialog
from PyQt6.QtGui import QDoubleValidator, QIcon
from PyQt6.QtCore import QLocale
from PyQt6.uic import loadUi
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg
from matplotlib.figure import Figure
from numpy import array
from PyQt6.uic import loadUi
from logger import logger
from resourcePath import resourcePath
from dialogs.messageDialog import messageDialog
import qtawesome as qta
from changeIconTheme import changeIconTheme

# --------------------------------------------------   
# Classe pour la boîte de dialogue de graphique
# -------------------------------------------------- 

class graphicDialog(QDialog):

    def __init__(self, data, nbPoints, ratio, etalonnage, settings, theme):
        super().__init__()

        logger.info("Affichage du graphique")

        logger.info("Chargement de import.ui")
        loadUi(resourcePath('assets/ui/graph.ui'), self)
        self.setWindowIcon(QIcon(resourcePath('assets/icons/icon.png')))
        self.setWindowTitle("Graphique")

        self.theme = theme

        self.setFixedSize(self.size())
        self.xList = data[1]
        self.yList = data[2]
        self.t = data[0]
       
        self.nbPoints = nbPoints
        if self.nbPoints >= 2:
            self.xList2 = data[3]
            self.yList2 = data[4]
        self.settings = settings

        self.ratio = ratio
        self.etalonnage = etalonnage


        self.bottom = self.ratio[2]*self.etalonnage["valeurMetres"]/self.etalonnage["valeurPixels"]
        self.top = self.ratio[3]*self.etalonnage["valeurMetres"]/self.etalonnage["valeurPixels"]
        self.left = self.ratio[0]*self.etalonnage["valeurMetres"]/self.etalonnage["valeurPixels"]
        self.right = self.ratio[1]*self.etalonnage["valeurMetres"]/self.etalonnage["valeurPixels"]

        if self.nbPoints == 2:
            self.comboAbs.addItem("t")
            self.comboAbs.addItem("x1")
            self.comboAbs.addItem("y1")
            self.comboAbs.addItem("x2")
            self.comboAbs.addItem("y2")
            self.comboAbs.addItem("x1-x2")
            self.comboOrd.addItem("t")
            self.comboOrd.addItem("x1")
            self.comboOrd.addItem("y1")
            self.comboOrd.addItem("x2")
            self.comboOrd.addItem("y2")
        else :
            self.comboAbs.addItem("t")
            self.comboAbs.addItem("x")
            self.comboAbs.addItem("y")
            self.comboOrd.addItem("t")
            self.comboOrd.addItem("x")
            self.comboOrd.addItem("y")

        # Ajout du plot au canvas
        self.figure = Figure()
        self.sc = FigureCanvasQTAgg(self.figure)
        self.figure.patch.set_facecolor("None")
        self.figure.tight_layout(pad=0)
        self.sc.axes = self.figure.add_subplot(111)

        # Gestion de la taille du plot
        sizePolicy = QSizePolicy(QSizePolicy.Policy.Expanding, QSizePolicy.Policy.Expanding)
        self.sc.setSizePolicy(sizePolicy)

        layout = QVBoxLayout()
        layout.addWidget(self.sc)
        self.saveButton.setAutoDefault(False)

        # Creation d'un widget contenant le canvas
        self.mainWidget= QWidget(self.mainGroup)
        self.sc.updateGeometry()
        self.sc.setContentsMargins(0, 0, 0, 0)
        self.mainWidget.setLayout(layout)

        self.sc.axes.plot(self.xList,self.yList,str(self.settings["color"]+self.settings["point"]+self.settings["line"]))

        self.comboAbs.setCurrentIndex(1)
        self.comboOrd.setCurrentIndex(2)

        self.textXmin.setEnabled(False)
        self.textXmax.setEnabled(False)
        self.textYmin.setEnabled(False)
        self.textYmax.setEnabled(False)
        self.adaptButton.setEnabled(False)

        self.textAbs.setText("Titre des abscisses")
        self.textOrd.setText("Titre des ordonnées")
        self.titre.setText("Titre du graphique")
        self.sc.axes.set_xlabel(self.textAbs.text())
        self.sc.axes.set_ylabel(self.textOrd.text())
        self.sc.axes.set_title(self.titre.text())

        self.textXmin.setText(str(min(x for x in self.xList if x is not None)))
        self.textXmax.setText(str(max(x for x in self.xList if x is not None)))
        self.textYmin.setText(str(min(y for y in self.yList if y is not None)))
        self.textYmax.setText(str(max(y for y in self.yList if y is not None)))
        
        self.minImageSpin.setMinimum(0)
        self.minImageSpin.setMaximum(len(self.xList))
        self.minImageSpin.setValue(0)
        self.minImage = 0

        self.maxImageSpin.setMinimum(0)
        self.maxImageSpin.setMaximum(len(self.xList))
        self.maxImageSpin.setValue(len(self.xList))
        self.maxImage = len(self.xList)

        self.onlyDouble = QDoubleValidator()
        self.onlyDouble.setLocale(QLocale("en_US"))
        self.textXmin.setValidator(self.onlyDouble)
        self.textXmax.setValidator(self.onlyDouble)
        self.textYmin.setValidator(self.onlyDouble)
        self.textYmax.setValidator(self.onlyDouble)

        self.checkBoxFenetre.stateChanged.connect(self.state_changed) 
        self.minImageSpin.valueChanged.connect(self.update_canvas)
        self.maxImageSpin.valueChanged.connect(self.update_canvas)
        self.textAbs.textChanged.connect(self.update_canvas)
        self.textOrd.textChanged.connect(self.update_canvas)
        self.titre.textChanged.connect(self.update_canvas)
        self.textXmin.textChanged.connect(self.update_canvas)
        self.textXmax.textChanged.connect(self.update_canvas)
        self.textYmin.textChanged.connect(self.update_canvas)
        self.textYmax.textChanged.connect(self.update_canvas)
        self.comboAbs.currentTextChanged.connect(self.update_canvas)
        self.comboOrd.currentTextChanged.connect(self.update_canvas)
        self.comboColor.currentTextChanged.connect(self.update_canvas)
        self.comboFormat.currentTextChanged.connect(self.update_canvas)
        self.checkBoxQuadrillage.stateChanged.connect(self.update_canvas)
        self.checkLine.stateChanged.connect(self.update_canvas)  
        self.saveButton.clicked.connect(self.save_plot)
        self.adaptButton.clicked.connect(self.adapt_plot)

        self.moveEvent = self.sc.mpl_connect('motion_notify_event',self.coord_update)


        self.saveButton.setIcon(qta.icon("fa5s.save",color="white"))

        # Appliquer la classe .blueBox au groupbox
        self.groupBox.setProperty("class", "blueBox")

        changeIconTheme(theme, self)

    def adapt_plot(self):
        self.textXmin.setText(str(self.left))
        self.textXmax.setText(str(self.right))
        self.textYmin.setText(str(self.bottom))
        self.textYmax.setText(str(self.top))
        self.sc.axes.set_xlim(float(self.textXmin.text()),float(self.textXmax.text()))
        self.sc.axes.set_ylim(float(self.textYmin.text()),float(self.textYmax.text()))
        self.update_canvas()

    def state_changed(self):
        if self.checkBoxFenetre.isChecked() == False:
            self.textXmin.setEnabled(True)
            self.textXmax.setEnabled(True)
            self.textYmin.setEnabled(True)
            self.textYmax.setEnabled(True)
            self.adaptButton.setEnabled(True)
        else:
            self.textXmin.setEnabled(False)
            self.textXmax.setEnabled(False)
            self.textYmin.setEnabled(False)
            self.textYmax.setEnabled(False)
            self.adaptButton.setEnabled(False)
            # self.textXmin.setText(str(min(x for x in self.xList if x is not None)))
            # self.textXmax.setText(str(max(x for x in self.xList if x is not None)))
            # self.textYmin.setText(str(min(y for y in self.yList if y is not None)))
            # self.textYmax.setText(str(max(y for y in self.yList if y is not None)))
            self.sc.axes.autoscale()
            self.sc.draw()
    
    def coord_update(self,event):
        if event.xdata!=None and event.ydata!=None :
                self.textCoord.setText("Coordonnées du curseur : "+str(round(event.xdata,2))+str(" ; ")+str(round(event.ydata,2)))

    def update_canvas(self):
        xdata=array([None for i in range(len(self.t))])
        ydata=array([None for i in range(len(self.t))])

        if self.comboAbs.currentText() == "t":
            xdata = self.t
        elif self.comboAbs.currentText() == "x" or self.comboAbs.currentText() == "x1":
            xdata = self.xList
        elif self.comboAbs.currentText() == "y" or self.comboAbs.currentText() == "y1":
            xdata = self.yList
        elif self.comboAbs.currentText() == "x2":
            xdata = self.xList2
        elif self.comboAbs.currentText() == "y2":
            xdata = self.yList2
        elif self.comboAbs.currentText() == "x1-x2":
            for i in range(len(xdata)):
                if self.xList[i] != None and self.xList2[i] != None:
                    xdata[i] = self.xList[i] - self.xList2[i]

        if self.comboOrd.currentText() == "t":
            ydata = self.t
        elif self.comboOrd.currentText() == "x" or self.comboOrd.currentText() == "x1":
            ydata = self.xList
        elif self.comboOrd.currentText() == "y" or self.comboOrd.currentText() == "y1":
            ydata = self.yList
        elif self.comboOrd.currentText() == "x2":
            ydata = self.xList2
        elif self.comboOrd.currentText() == "y2":
            ydata = self.yList2

        colorValue = self.comboColor.currentText()
        if colorValue == "Bleu":
            self.settings["color"] = "b"
        elif colorValue == "Rouge":
            self.settings["color"] = "r"
        elif colorValue == "Vert":
            self.settings["color"] = "g"
        elif colorValue == "Cyan":
            self.settings["color"] = "c"
        elif colorValue == "Magenta":
            self.settings["color"] = "m"
        elif colorValue == "Jaune":
            self.settings["color"] = "y"
        elif colorValue == "Blanc":
            self.settings["color"] = "w"
        elif colorValue == "Noir":
            self.settings["color"] = "k"
        else:
            self.settings["color"] = "b"

        formeValue = self.comboFormat.currentText()
        if formeValue == "Point":
            self.settings["point"] = "."
        elif formeValue == "Disque":
            self.settings["point"] = "o"
        elif formeValue == "Croix":
            self.settings["point"] = "x"
        elif formeValue == "Plus":
            self.settings["point"] = "+"
        elif formeValue == "Carré":
            self.settings["point"] = "s"
        elif formeValue == "Triangle":
            self.settings["point"] = "v"
        else:
            self.settings["point"] = "o"
        
        if self.checkLine.isChecked():
            self.settings["line"] = "--"
        else:
            self.settings["line"] = ""

        self.minImage = self.minImageSpin.value()
        self.minImageSpin.setMinimum(0)
        self.minImageSpin.setMaximum(len(xdata))
        if self.minImageSpin.value() >= len(xdata):
            self.minImageSpin.setValue(len(xdata))
            self.minImage = len(xdata)

        self.maxImage = self.maxImageSpin.value()
        self.maxImageSpin.setMinimum(0)
        self.maxImageSpin.setMaximum(len(xdata))
        if self.maxImageSpin.value() >= len(xdata):
            self.maxImageSpin.setValue(len(xdata))
            self.maxImage = len(xdata)

        self.sc.axes.clear()
        self.sc.axes.plot(xdata[self.minImage:self.maxImage+1],ydata[self.minImage:self.maxImage+1],str(self.settings["color"]+self.settings["point"]+self.settings["line"]))
        self.sc.axes.set_xlabel(self.textAbs.text())
        self.sc.axes.set_ylabel(self.textOrd.text())
        self.sc.axes.set_title(self.titre.text())
        
        if self.checkBoxFenetre.isChecked() == True:
            self.textXmin.setText(str(min(x for x in xdata if x is not None)))
            self.textXmax.setText(str(max(x for x in xdata if x is not None)))
            self.textYmin.setText(str(min(y for y in ydata if y is not None)))
            self.textYmax.setText(str(max(y for y in ydata if y is not None)))
        else:
            if self.textXmin.text() != '-' and self.textXmin.text() != "+" and self.textXmax.text() != '-' and self.textXmax.text() != "+" and self.textXmin.text() != '' and self.textXmax.text() != "":
                self.sc.axes.set_xlim(float(self.textXmin.text()),float(self.textXmax.text()))
            if self.textYmin.text() != '-' and self.textYmin.text() != "+" and self.textYmax.text() != '-' and self.textYmax.text() != "+" and self.textYmin.text() != '' and self.textYmax.text() != "":
                self.sc.axes.set_ylim(float(self.textYmin.text()),float(self.textYmax.text()))

        if self.checkBoxQuadrillage.isChecked() == True:
            self.sc.axes.grid(True)
        else:
            self.sc.axes.grid(False)

        self.sc.draw()

    def save_plot(self):
        filePath, _ = QFileDialog.getSaveFileName(self, "Image", "",
                            "PNG(*.png);;JPEG(*.jpg *.jpeg);;All Files(*.*) ")
    
        if filePath == "":
            return
        
        try :
            # Sauvegarde du canvas
            self.sc.print_figure(filePath)
            logger.info("Écriture du fichier png terminée avec succès")
        except Exception as ex:
            logger.warning("Une erreur est survenue : " + ex) 

    def closeEvent(self, event):
        dlg = messageDialog("Voulez-vous vraiment quitter ? Votre configuration du graphique sera perdue.", self.theme)
        if dlg.exec():
            event.accept()
        else:
            event.ignore()
