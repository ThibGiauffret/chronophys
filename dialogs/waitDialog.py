from PyQt6.QtCore import Qt, QEvent
from PyQt6.QtWidgets import QDialog, QVBoxLayout, QLabel
from PyQt6.QtGui import QIcon
from waitingSpinnerWidget import QtWaitingSpinner
from logger import logger
from changeIconTheme import changeIconTheme
from resourcePath import resourcePath

# --------------------------------------------------   
# Classe pour la boîte de dialogue d'attente
# -------------------------------------------------- 

class waitDialog(QDialog):

    def __init__(self, theme):
        super().__init__()
        logger.info("Affichage de WaitDialog")

        self.setWindowTitle("Importation en cours...")
        self.setWindowIcon(QIcon(resourcePath('assets/icons/icon.png')))
        self.setFixedSize(300, 150)

        self.layout = QVBoxLayout()

        message = QLabel("Veuiilez patienter...")
        message.setAlignment(Qt.AlignmentFlag.AlignCenter)

        spinner = QtWaitingSpinner(self)

        spinner.setRoundness(70.0)
        spinner.setMinimumTrailOpacity(15.0)
        spinner.setTrailFadePercentage(70.0)
        spinner.setNumberOfLines(12)
        spinner.setLineLength(10)
        spinner.setLineWidth(5)
        spinner.setInnerRadius(10)
        spinner.setRevolutionsPerSecond(1)
        if theme == "dark":
            spinner.setColor(Qt.GlobalColor.white)
        else :
            spinner.setColor(Qt.GlobalColor.black)
        self.layout.addWidget(spinner)
        self.layout.addWidget(message)
        self.setLayout(self.layout)

        spinner.start()

        changeIconTheme(theme, self)
  

    def start(self):
        self.setWindowModality(Qt.WindowModality.WindowModal)
        self.show()
        return True

    def stop(self):
        logger.info("Fermeture de WaitDialog")
        self.done(0)

    def closeEvent(self, evnt):
        evnt.ignore()