import os
import sys

def getCSS(theme):

    try:
        application_path = sys._MEIPASS
    except Exception:
        application_path = os.path.abspath(".")

    if theme == "dark":
        # Open the assets/dark.css file
        css = os.path.join(application_path, "assets", "dark.css")

        # Read the content of the dark.css file
        with open(css, "r") as file:
            dark_css_content = file.read()
            # Replace "assets/icons" by the relative path of the icons folder
            dark_css_content = dark_css_content.replace("assets/icons/", os.path.join(application_path, "assets", "icons", "")).replace("\\", "/")

            return dark_css_content
    else :
        # Open the assets/light.css file
        css = os.path.join(application_path, "assets", "light.css")

        # Read the content of the light.css file
        with open(css, "r") as file:
            light_css_content = file.read()
            # Replace "assets/icons" by the relative path of the icons folder
            light_css_content = light_css_content.replace("assets/icons/", os.path.join(application_path, "assets", "icons", "")).replace("\\", "/")

            return light_css_content


