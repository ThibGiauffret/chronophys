# ChronoPhys est un logiciel libre pour réaliser des chronophotographies en Sciences-Physiques
# Licence : GNU GPLv3 
# Auteur : Thibault Giauffret, ensciences.fr (2022-2024)
# Version : dev-beta v0.9.3 (03 juin 2024)

version_number = "dev-beta v0.9.3 (03 juin 2024)"

# --------------------------------------------------   
# Importation des librairies
# -------------------------------------------------- 
import sys, os, csv

# Gestion de l'interface
from PyQt6.QtCore import (
    QObject,
    pyqtSignal,
    QLocale,
    QRect,
    QPoint,
    QThread,
    Qt
)
from PyQt6.QtGui import (
    QIcon,
    QDoubleValidator,
    QPixmap,
    QPainter,
    QPen,
    QBrush,
    QImage,
    QColor
)
from PyQt6.QtWidgets import (
    QMainWindow,
    QSizePolicy,
    QVBoxLayout,
    QWidget,
    QFileDialog,
    QApplication,
    QDialog,
    QTableWidgetItem,
    QPushButton,
    QToolButton,
    QSystemTrayIcon
)
from PyQt6.uic import loadUi
import qtawesome as qta
import darkdetect

# Gestion des délais (pour la lecture)
from time import sleep

# Gestion de la copie dans le presse-papier
from pyperclip import copy as pccopy

# Gestion des tableaux et des listes
from numpy import (linspace, array,zeros, int8)

# Gestion des graphiques et du canvas
import matplotlib
matplotlib.use('Qt5Agg')
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg
from matplotlib.figure import Figure

# Gestion de l'importation avec OpenCV
from extract import extract_infos
from webserver import have_internet

# Récupération du chemin absolu vers l'application
from pathlib import Path
if getattr(sys, 'frozen', False):
    application_path = str(Path(os.path.dirname(os.path.realpath(sys.executable))))
elif __file__:
    application_path = str(Path(os.path.dirname(os.path.realpath(__file__))))

import webbrowser

# Importation des modules de l'application
from dialogs.graphicDialog import graphicDialog
from dialogs.messageDialog import messageDialog
from dialogs.webDialog import webDialog
from dialogs.waitDialog import waitDialog
from dialogs.importDialog import importDialog
from dialogs.webcamDialog import webcamDialog
from workers.importWorker import importWorker
from workers.webWorker import webWorker
from resourcePath import resourcePath
from logger import logger
from changeIconTheme import changeIconTheme
from getCSS import getCSS

# --------------------------------------------------   
# Classe principale gérant l'application
# -------------------------------------------------- 
class Window(QMainWindow):
    # --------------------------------------------------
    # Evènement de d'appui sur la touche "Del"
    # --------------------------------------------------
    def keyPressEvent(self, event):
        if event.key() == Qt.Key.Key_Delete:
            # On vérifie que le tableau n'est pas vide et que l'onglet "Mesures" est actif
            if self.tableWidget.rowCount() == 0 or self.tabWidget.currentIndex() != 1:
                return
            # On récupère la ligne sélectionnée
            row = self.tableWidget.currentRow()
            # On supprime les données de la ligne
            self.data_brute[0][row] = None
            self.data_brute[1][row] = None
            self.data_brute[2][row] = None
            self.data[0][row] = None
            self.data[1][row] = None
            self.data[2][row] = None
            self.data_brute[3][row] = None
            self.data_brute[4][row] = None
            self.data[3][row] = None
            self.data[4][row] = None
            # On supprime la ligne du tableau
            self.tableWidget.setItem(row, 0, QTableWidgetItem(""))
            self.tableWidget.setItem(row, 1, QTableWidgetItem(""))
            self.tableWidget.setItem(row, 2, QTableWidgetItem(""))
            # Si on est en mode 2 points, on supprime les données des colonnes 3 et 4
            if self.nbPoints == 2:
                self.tableWidget.setItem(row, 3, QTableWidgetItem(""))
                self.tableWidget.setItem(row, 4, QTableWidgetItem(""))

            self.canvas_update()
        # If key is alt show the menu bar
        elif event.key() == Qt.Key.Key_Alt:
            if self.menuBar().isHidden():
                self.menuBar().show()
            else:
                self.menuBar().hide()
        else:
            # On laisse la gestion des autres touches au système
            super().keyPressEvent(event)

    # --------------------------------------------------   
    # Initialisation de la fenêtre
    # --------------------------------------------------  
    def __init__(self):
        super().__init__()
        logger.info("Affichage de la fenêtre principale")

        # Importation de l'interface de base
        loadUi(resourcePath('assets/ui/main.ui'), self)
        self.setWindowIcon(QIcon(resourcePath('assets/icons/icon.png')))
        logger.info("Chargement de l'interface main.ui réalisée avec succès")

        # Initialisation des variables
        self.theme = "dark" if darkdetect.isDark() else "light"
        self.mesures = False
        self.data_brute = array([])
        self.data = array([])
        self.images = []
        self.nbPoints = 1
        self.sous_mesure = 0
        self.loupe = False
        self.newopen = False
        self.webserver_running = False
        self.version = "<b>ChronoPhys</b> est un logiciel libre pour réaliser des chronophotographies en Sciences-Physiques<br><br><b>Licence</b> : GNU GPLv3 <br><b>Auteur</b> : Thibault Giauffret, <a href=\"https://ensciences.fr\">ensciences.fr</a>(2022-2024)<hr><b>Version</b> : "+version_number+"<br><b>Contact</b> : <a href=\"mailto:contact@ensciences.fr\">contact@ensciences.fr</a>"

        # Ajout du plot au canvas
        self.figure = Figure()
        self.sc = FigureCanvasQTAgg(self.figure)
        self.figure.patch.set_facecolor("None")
        self.figure.tight_layout(pad=0)
        self.sc.axes = self.figure.add_subplot(111)

        # Gestion de la taille du plot
        sizePolicy = QSizePolicy(QSizePolicy.Policy.Expanding, QSizePolicy.Policy.Expanding)
        self.sc.setSizePolicy(sizePolicy)

        # Affichage de l'image initiale
        self.sc.setStyleSheet("background-color:transparent;")
        img = matplotlib.image.imread(resourcePath('assets/icons/stopwatch.png'))
        self.sc.axes.imshow(img, extent=[-img.shape[1]/2., img.shape[1]/2., -img.shape[0]/2., img.shape[0]/2. ])
        self.sc.axes.set_axis_off()
        self.sc.axes.margins(0)
        self.clickEvent = self.sc.mpl_connect('button_press_event',self.measure_event)
        self.moveEvent = self.sc.mpl_connect('motion_notify_event',self.loupe_update)

        layout = QVBoxLayout()
        layout.addWidget(self.sc)

        # Creation d'un widget contenant le canvas
        self.mainWidget= QWidget(self.mainGroup)
        self.sc.updateGeometry()
        self.sc.setContentsMargins(0, 0, 0, 0)
        self.mainWidget.setLayout(layout)
        
        # Mise en arrière plan du canvas
        self.sc.lower()
 
        # Préparation des autres éléments de l'interface (activation, remplacement des libellés, utilisation d'icônes...)
        self.tabWidget.setTabEnabled(1, False)
        self.tabWidget.setCurrentIndex(0)
        self.openButton.clicked.connect(self.video_open)
        self.phoneButton.clicked.connect(self.smartphone_video_open)
        self.webcamButton.clicked.connect(self.webcam_video_open)
        self.graphiqueButton.clicked.connect(self.graphique_open)
        self.actionOuvrir_un_fichier_vid_o.triggered.connect(self.video_open)
        self.actionEnregistrer_une_vid_o_avec_un_smartphone.triggered.connect(self.smartphone_video_open)
        self.actionEnregistrer_une_vid_o_avec_la_webcam.triggered.connect(self.webcam_video_open)
        self.loupeBox.hide()

        self.playButton.setText('')
        self.playButton.setIcon(qta.icon('fa5s.play', color='white'))
        self.pauseButton.setText('')
        self.pauseButton.setIcon(qta.icon('fa5s.pause', color='white'))
        self.nextButton.setText('')
        self.nextButton.setIcon(qta.icon('fa5s.forward', color='white'))
        self.prevButton.setText('')
        self.prevButton.setIcon(qta.icon('fa5s.backward', color='white'))
        self.menuButton.setText('')
        self.menuButton.setIcon(qta.icon('fa5s.bars', color='white'))
        
        self.openButton.setIcon(qta.icon('fa5s.video', color='white'))

        self.webcamButton.setIcon(qta.icon('fa5s.camera', color='white'))
        
        self.phoneButton.setIcon(qta.icon('fa5s.mobile', color='white'))
 
        self.saveButton.setText('')
        self.saveButton.setProperty('class', 'greenBtn')
        self.saveButton.setIcon(qta.icon('fa5s.save', color='white'))
        self.loupeButton.setText('')
        self.loupeButton.setIcon(qta.icon('fa5s.search', color='white'))
        
        self.validateButton.setIcon(qta.icon('fa5s.check-circle', color='white'))
        self.rulerButton.setIcon(qta.icon('fa5s.ruler', color='white'))
        self.repereButton.setIcon(qta.icon('fa5s.bullseye', color='white'))
        self.formeButton.setIcon(qta.icon('fa5s.paint-brush', color='white'))
        self.imageLabel.setText('')

        self.axeButton_1.setIcon(self.icon_from_svg(resourcePath("assets/icons/axis1.svg")))
        self.axeButton_3.setIcon(self.icon_from_svg(resourcePath("assets/icons/axis4.svg")))
        self.axeButton_4.setIcon(self.icon_from_svg(resourcePath("assets/icons/axis2.svg")))
        self.axeButton_5.setIcon(self.icon_from_svg(resourcePath("assets/icons/axis3.svg")))

        self.etalonBox.setEnabled(False)
        self.repereBox.setEnabled(False)
        self.styleBox.setEnabled(False)
        self.validateButton.setEnabled(False)
        self.tabWidget.setTabEnabled(2, True)
        self.graphiqueButton.setEnabled(False)
        
        self.onlyDouble = QDoubleValidator()
        self.onlyDouble.setLocale(QLocale("en_US"))
        self.valeurEtalon.setValidator(self.onlyDouble)

        self.action_propos.triggered.connect(self.infos_clicked)
        self.actionTutoriel_en_ligne.triggered.connect(self.tutoriel_clicked)
        self.actionSignaler_un_bug.triggered.connect(self.bug_clicked)
        self.tutoButton.clicked.connect(self.tutoriel_clicked)
        self.bugButton.clicked.connect(self.bug_clicked)
        self.actionSombre.triggered.connect(lambda: self.theme_clicked("dark"))
        self.actionClair.triggered.connect(lambda: self.theme_clicked("light"))
        self.tutoButton.setIcon(qta.icon('fa5s.book', color='white'))
        self.bugButton.setIcon(qta.icon('fa5s.bug', color='white'))
        self.playButton.clicked.connect(self.play)
        self.menuButton.clicked.connect(lambda: self.menu_clicked())

        # Complétion des informations
        self.pixmap = QPixmap(resourcePath("assets/icons/ensciences.svg")).scaled(78, 78, Qt.AspectRatioMode.KeepAspectRatio, Qt.TransformationMode.SmoothTransformation)
        self.label_ensciences.setPixmap(self.pixmap) 
        self.label_ensciences.resize(80,80) 

        self.pixmap = QPixmap(resourcePath("assets/icons/icon.svg")).scaled(78, 78, Qt.AspectRatioMode.KeepAspectRatio, Qt.TransformationMode.SmoothTransformation)
        self.label_chrono.setPixmap(self.pixmap) 
        self.label_chrono.resize(80,80) 

        self.label_infos.setText(self.version)
        self.label_infos.setTextFormat(Qt.TextFormat.RichText)
        self.label_infos.setOpenExternalLinks(True)

        # Affichage de la fenêtre
        self.show()

        self.loupeButton.clicked.connect(self.loupe_clicked)
        self.canvas_resize()

        # Application du style CSS
        self.openButton.setProperty('class', 'greenBtn')
        self.phoneButton.setProperty('class', 'greenBtn')
        self.webcamButton.setProperty('class', 'greenBtn')
        self.repereBox.setProperty('class', 'blueBox')
        self.styleBox.setProperty('class', 'blueBox')
        self.etalonBox.setProperty('class', 'blueBox')
        self.infoBox.setProperty('class', 'blueBox')
        self.creditBox.setProperty('class', 'blueBox')
        app.setStyleSheet(app.styleSheet())

        # Set the theme
        self.theme_clicked(self.theme)

        # Hide the menu bar
        self.menuBar().hide()

        # Show tray icon
        self.tray_icon = QSystemTrayIcon(self)
        self.tray_icon.setIcon(QIcon(resourcePath('assets/icons/icon.png')))
        self.tray_icon.setToolTip("ChronoPhys")
        self.tray_icon.show()

        logger.info("Fin de l'initialisation de l'interface")

    # --------------------------------------------------   
    # Complétion de l'interface une fois la video ouverte
    # --------------------------------------------------  

    def ui_update(self):

        logger.info("Mise à jour de l'interface")

        self.show_toast("Succès !", "La vidéo a été importée avec succès. Vous pouvez maintenant commencer la préparation pour le pointage.")

        self.canvas_resize()

        try:
            self.sc.mpl_disconnect(self.clickEvent)
        except:
            logger.warning("Impossible de déconnecter l'évènement de clic sur canvas. Déjà déconnecté ?!") 

        self.label_nombre.setText(str(self.videoConfig["nb_images"]))
        self.label_ips.setText(str(self.videoConfig["fps"]))
        self.label_duree.setText(str(self.videoConfig["duration"]))
        self.label_largeur.setText(str(self.videoConfig["width"]))
        self.label_hauteur.setText(str(self.videoConfig["height"]))        
        
        self.repereBox.setProperty('class', 'greenBox')
        self.repereBox.setEnabled(True)

        # Supprimer la classe greenBtn de tous les boutons
        self.openButton.setProperty('class', '')
        self.phoneButton.setProperty('class', '')
        self.webcamButton.setProperty('class', '')

        # Application du style CSS
        app.setStyleSheet(app.styleSheet())

        changeIconTheme(self.theme, self)

        self.buttonGroup.setExclusive(False)
        self.firstPoint.setChecked(False)
        self.secondPoint.setChecked(False)
        self.buttonGroup.setExclusive(True)
        self.etalonBox.setEnabled(False)

        self.styleBox.setEnabled(False)

        # Chargement dans le canvas de la première image de la vidéo
        self.sc.axes.cla()
        self.figure.subplots_adjust(bottom=0, right=1, top=1, left=0)
        self.mywidth = self.images[self.current_image].shape[0]
        self.myheight = self.images[self.current_image].shape[1]
        self.Vaxis_orient = 1
        self.Haxis_orient = 1
        self.Vaxis_ratio = 0.5
        self.Haxis_ratio = 0.5
        self.old_axisParam = (1,1)
        self.ratio = [-self.Vaxis_ratio*self.mywidth,self.Vaxis_ratio*self.mywidth,-self.Haxis_ratio*self.myheight,self.Haxis_ratio*self.myheight]

        self.playStatus = False
        self.axis_set = False
        self.etalonnage = {"status":"stage1", "done":False, "x1":0, "y1":0,"x2":0,"y2":0,"valeurMetres":0,"valeurPixels":0}
        self.axisType = 1
        self.showEtalon = False
        self.applyOrient = False
        self.settings = {"color":"b","line":"","point":".","grid":False,"ticks":True}

        bottom = -self.mywidth*self.Haxis_ratio
        top = self.mywidth-abs(bottom)
        left = -self.myheight*self.Vaxis_ratio
        right = self.myheight-abs(left)
        # print(self.Haxis_orient*left,self.Haxis_orient*right,self.Vaxis_orient*bottom,self.Vaxis_orient*top)
        self.myextent=[self.Haxis_orient*left, self.Haxis_orient*right,self.Vaxis_orient*bottom, self.Vaxis_orient*top]
        self.ratio = [left,right,bottom,top]

        self.sc.axes.imshow(self.images[0], extent=[ -self.myheight*self.Vaxis_ratio, self.myheight*(1-self.Vaxis_ratio),-self.mywidth*self.Haxis_ratio, self.mywidth*(1-self.Haxis_ratio)])
        self.sc.axes.margins(0)
        self.sc.setContentsMargins(0, 0, 0, 0)

        self.sc.draw_idle()

        self.horizontalSlider.setRange(1, self.nb_images)

        self.axis_update()
        
        # Configuration des boutons 
        if self.newopen == False:
            self.pauseButton.clicked.connect(self.pause)
            self.nextButton.clicked.connect(self.next_clicked)
            self.prevButton.clicked.connect(self.prev_clicked)
            self.validateButton.clicked.connect(self.start_measures)
            self.axeButton_1.clicked.connect(lambda: self.axe_clicked(1))
            self.axeButton_3.clicked.connect(lambda: self.axe_clicked(2))
            self.axeButton_4.clicked.connect(lambda: self.axe_clicked(3))
            self.axeButton_5.clicked.connect(lambda: self.axe_clicked(4))
            self.repereButton.clicked.connect(self.repere_clicked)
            self.firstPoint.toggled.connect(lambda: self.etalonnage_clicked(1))
            self.secondPoint.toggled.connect(lambda: self.etalonnage_clicked(2))
            self.rulerButton.clicked.connect(self.ruler_clicked)
            self.horizontalSlider.valueChanged.connect(self.slider_update)
            self.saveButton.clicked.connect(lambda: self.save_clicked(self.comboBox.currentIndex()))
            # Clic sur une cellule du tableau
            self.tableWidget.clicked.connect(self.row_changed)
            # Clic sur l'entête du tableau
            self.tableWidget.verticalHeader().sectionClicked.connect(self.row_changed_header)
            self.tabWidget.tabBarClicked.connect(self.tabbar_clicked)
            self.tableWidget.itemDoubleClicked.connect(self.table_clicked)
            self.formeButton.clicked.connect(self.settings_update)

            # Triggers dans le menubar
            self.actionCopier_dans_le_presse_papier.triggered.connect(lambda: self.save_clicked(0))
            self.actionCopier_dans_le_presse_papier_listes_Python.triggered.connect(lambda: self.save_clicked(1))
            self.actionSauvergarder_au_format_csv.triggered.connect(lambda: self.save_clicked(2))
            self.actionEnregistrer_le_code_Python_py.triggered.connect(lambda: self.save_clicked(3))
            self.actionExporter_le_graphique.triggered.connect(lambda: self.save_clicked(4))

        # Modification du widget tableau pour les valeurs
        self.tableWidget.setRowCount(0)
        self.tableWidget.setColumnCount(3)
        self.tableWidget.setRowCount(self.nb_images) 
        self.tableWidget.setHorizontalHeaderLabels(["Temps (s)", "x (m)", "y (m)" ]) 

        logger.info("Mise à jour de l'interface réussie")

    # --------------------------------------------------   
    # Gestion du redimensionnement de la fenêtre
    # -------------------------------------------------- 

    def canvas_resize(self):
        self.mainWidget.setGeometry(QRect(0, 0, self.mainGroup.width(), self.mainGroup.height()))
        self.sc.setGeometry(QRect(0, 0, self.mainGroup.width(), self.mainGroup.height()))

    def resizeEvent(self, event):
        self.canvas_resize()
    
    # --------------------------------------------------   
    # Gestion de l'ouverture de la vidéo
    # --------------------------------------------------  

    def video_open(self):
        logger.info("Clic openButton")
        # Open the native file dialog
        dialog = QFileDialog(self)
        dialog.setNameFilter(str("Video (*.mp4 *.avi *.wmv *mov);;All Files (*.*)"))
        dialog.setDirectory(os.getenv('HOME'))
        if dialog.exec():
            filename = dialog.selectedFiles()[0]
            logger.info("Vidéo sélectionnée : "+filename)
            self.import_video(filename)
        else:
            logger.info("Importation annulée")

    def get_import_data(self, images, videoConfig, error, video_timestamp):
        logger.info("Importation des données de la vidéo")
        self.dlg_wait.stop()
        if error == False:
            self.images = images
            self.videoConfig = videoConfig
            self.video_timestamp = video_timestamp
            self.nb_images = self.videoConfig["nb_images"]
            # print(self.nb_images)
            self.duration = self.videoConfig["duration"]
            self.current_image = 0
            #print(self.images[self.current_image])
            self.imageLabel.setText('Image : '+str(self.current_image+1)+'/'+str(self.nb_images))
            try: self.horizontalSlider.valueChanged.disconnect()
            except Exception as ex: 
                logger.warning("Impossible de déconnecter l'évènement itemChanged sur tableWidget. Déjà déconnecté ?!")
            self.horizontalSlider.setValue(self.current_image+1)
            self.horizontalSlider.valueChanged.connect(self.slider_update)
            self.data_brute = array([[None for i in range(self.nb_images)] for j in range(5)])
            self.data = array([[None for i in range(self.nb_images)] for j in range(5)])
            self.valeurEtalon.setText("")
            self.ui_update()
            self.newopen = True
        else:
            dlg = messageDialog("Une erreur est survenue lors de l'ouverture de la vidéo. Assurez-vous qu'elle contienne moins de 150 images.", self.theme)
            logger.warning("La vidéo contient trop d'images...")
            if dlg.exec():
                print("Success!")
            else:
                print("Cancel!")

            
    # --------------------------------------------------   
    # Gestion du serveur web (envie video depuis smartphone)
    # --------------------------------------------------  

    def smartphone_video_open(self):

        logger.info("Importation d'une vidéo depuis un smartphone")
        if self.webserver_running == False:
            try :
                if have_internet():
                    logger.info("Ping internet OK, lancement du thread avec WebWorker")
                    # On crée le QThread object
                    self.web_thread = QThread()
                    self.web_thread.setTerminationEnabled(True)

                    # On crée l'objet "Worker"
                    self.web_worker = webWorker()

                    # On déplace le worker dans le thread
                    self.web_worker.moveToThread(self.web_thread)

                    # On connecte les signaux et les slots
                    self.web_thread.started.connect(self.web_worker.run)
                    self.web_worker.finished.connect(self.stop_webserver)
                    self.web_worker.finished.connect(self.web_thread.quit)
                    self.web_worker.finished.connect(self.web_worker.deleteLater)
                    self.web_thread.finished.connect(self.web_thread.deleteLater)

                    self.web_worker.video.connect(self.video_received)

                    # On démarre le thread
                    self.web_thread.start()

                    self.webserver_running = True

                    self.server_dlg = webDialog(application_path, self.theme)
                    if self.server_dlg.exec():
                        print("Success!")
                    else:
                        print("Cancel!")
                else :
                    logger.warning("Aucune connexion internet disponible, impossible de démarrer le serveur web")
                    dlg = messageDialog("Aucune connexion internet n'est disponible. Impossible de démarrer le serveur web pour l'acquisition à l'aide d'un smartphone ou d'une tablette.." , self.theme)
                    if dlg.exec():
                        print("Success!")
                    else:
                        print("Cancel!")
                    
            except Exception as ex:
                logger.exception("Une erreur est survenue : " + str(ex))
        else :
            self.server_dlg = webDialog(application_path, self.theme)
            if self.server_dlg.exec():
                print("Success!")
            else:
                print("Cancel!")
        

    def stop_webserver(self):
        self.webserver_running = False

    def video_received(self,filename):
        logger.info("Réception d'une vidéo depuis le serveur web")
        filename = "./videos/"+filename
        dlg = messageDialog("Une vidéo a été reçue, voulez-vous l'ouvrir ?" , self.theme)
        if dlg.exec():
            self.server_dlg.stop()
            self.import_video(filename)
        else:
            logger.info("Importation refusée")

    def import_video(self, filename):
        logger.info("Importation de la vidéo")
        try :
            logger.info("Extraction des informations de la vidéo")
            first_frame, last_frame, camera_Width,camera_Height ,fps,frame_count,duration, error, error_message = extract_infos(str(filename))
            if error == False:
                logger.info("Informations extraites avec succès")
                dlg2 = importDialog(first_frame, last_frame,camera_Width,camera_Height ,fps,frame_count,duration, self.theme, str(filename))
                if dlg2.exec() == QDialog.DialogCode.Accepted:

                    self.dlg_wait = waitDialog(self.theme)

                    if self.dlg_wait.start():
                        value = dlg2.GetValue()
                        
                        try :
                            # On crée le QThread object
                            self.import_thread = QThread()
                            self.import_thread.setTerminationEnabled(True)

                            # On crée l'objet "Worker"
                            self.import_worker = importWorker(filename, value)

                            # On déplace le worker dans le thread
                            self.import_worker.moveToThread(self.import_thread)

                            # On connecte les signaux et les slots
                            self.import_thread.started.connect(self.import_worker.run)
                            self.import_worker.finished.connect(self.stop)
                            self.import_worker.finished.connect(self.import_thread.quit)
                            self.import_worker.finished.connect(self.import_worker.deleteLater)
                            self.import_thread.finished.connect(self.import_thread.deleteLater)

                            self.import_worker.data.connect(self.get_import_data)

                            # On démarre le thread
                            self.import_thread.start()
                        except Exception as ex:
                            logger.exception("Une erreur est survenue : " + str(ex))
                    else:
                        logger.warning("Importation annulée")
                        
                else:
                    logger.info("Importation refusée")
            else:
                logger.warning("Une erreur est survenue lors de l'extraction des informations de la vidéo : " + error_message)
                dlgError = messageDialog("Une erreur est survenue lors de l'extraction des informations de la vidéo. Veuillez vérifier le fichier...", self.theme)
                if dlgError.exec():
                    print("Success!")
                else:
                    print("Cancel!")
        except Exception as ex :
            logger.exception("Une erreur est survenue : " + str(ex))

    # --------------------------------------------------   
    # Gestion de la webcam
    # --------------------------------------------------  
    def webcam_video_open(self):
        self.webcam_dlg = webcamDialog(application_path, self.theme)
        self.webcam_dlg.exec()
        
        if self.webcam_dlg.recordDone == True and self.webcam_dlg.OK == True:
            logger.info("Enregistrement webcam confirmé")
            print("Success!")
            self.webcam_dlg.release()
            self.import_video(self.webcam_dlg.video_path)
        else:
            logger.info("Enregistrement webcam annulé")
            self.webcam_dlg.release()


    # --------------------------------------------------   
    # Gestion du dialogue "Graphique"
    # --------------------------------------------------  
    def graphique_open(self):

        self.graphique_dlg = graphicDialog(self.data, self.nbPoints, self.ratio, self.etalonnage, self.settings, self.theme)
        
        if self.graphique_dlg.exec():
            logger.info("Affichage du graphique")
            print("Success!")
        else:
            logger.info("Affichage du graphique annulé")


    # --------------------------------------------------   
    # Gestion des contrôles
    # --------------------------------------------------  

    def play(self):
        logger.info("Clic playButton, lecture de la vidéo")
        if self.images != []:
            self.playButton.setEnabled(False)
            self.scrollArea.setEnabled(False)
            self.playStatus = True

            try :
                # On crée le QThread object
                self.mythread = QThread()
                self.mythread.setTerminationEnabled(True)


                # On crée l'objet "Worker"
                self.worker = Worker(images= self.images, data_brute=self.data_brute, nbPoints=self.nbPoints, axes=self.sc.axes,myextent = self.myextent,etalonnage=self.etalonnage,showEtalon=self.showEtalon,ratio=self.ratio,settings=self.settings, current_image=self.current_image, nb_images=self.nb_images)

                # On déplace le worker dans le thread
                self.worker.moveToThread(self.mythread)

                # On connecte les signaux et les slots
                self.mythread.started.connect(self.worker.run)
                self.worker.finished.connect(self.stop)
                self.worker.finished.connect(self.mythread.quit)
                self.worker.finished.connect(self.worker.deleteLater)
                self.mythread.finished.connect(self.mythread.deleteLater)

                self.worker.data.connect(self.play_update)

                # On démarre le thread
                self.mythread.start()
            except Exception as ex:
                logger.exception("Une erreur est survenue : " + str(ex))

    def stop(self):
        logger.info("Arrêt de la lecture")
        self.playStatus = False
        self.playButton.setEnabled(True)
        self.scrollArea.setEnabled(True)

    def pause(self):
        try:
            self.worker.stop()
        except:
            logger.warning("Tentative d'arrêt de la lecture. Déjà arrêté ?")
        

    def next_clicked(self):
        logger.info("Clic nextButton")
        if self.current_image < self.nb_images-1 and self.playStatus == False:
            self.current_image+=1
            self.horizontalSlider.setValue(self.current_image+1)
            self.canvas_update()

    def prev_clicked(self):
        logger.info("Clic prevButton")
        if self.current_image > 0 and self.playStatus == False:
            self.current_image-=1
            self.horizontalSlider.setValue(self.current_image+1)
            self.canvas_update()

    # --------------------------------------------------   
    # Gestion de la loupe
    # --------------------------------------------------  

    def loupe_clicked(self):
        logger.info("Clic loupeButton")
        if self.loupe == False :
            self.loupeBox.show()
            self.loupe = True
        else:
            self.loupeBox.hide()
            self.loupe = False

    def loupe_update(self,event):
        if self.loupe == True and self.images !=[]:
            bottom = self.mywidth*(1-self.Haxis_ratio)
            left = self.myheight*(self.Vaxis_ratio)

            if event.xdata!=None and event.ydata!=None :
                posx = (abs(left)+(self.Haxis_orient*event.xdata))+1
                posy = (abs(bottom)-(self.Vaxis_orient*event.ydata))+1

                axe = QPixmap(135, 135)
                axe.fill(Qt.GlobalColor.transparent)          
                p = QPainter(axe)
                pen = QPen(QBrush(QColor(10,10,10,230)), 2)
                p.setPen(pen)
                p.drawLine(135, int(135/2), 0, int(135/2))
                p.drawLine(int(135/2),135, int(135/2),0)
                p.end()
                axe.scaled(135, 135,Qt.AspectRatioMode.KeepAspectRatio, Qt.TransformationMode.FastTransformation)


                image = zeros((int(0.1*self.mywidth)+1,int(0.1*self.mywidth)+1,3), int8)

                for row in range(0,int(0.1*self.mywidth)):
                    for col in range(0,int(0.1*self.mywidth)):
                        if int(posx-0.05*self.mywidth+col) <= 0 or int(posx-0.05*self.mywidth+col) >= self.myheight or int(posy-0.05*self.mywidth+row) >= self.mywidth :
                            image[row][col] = [0,0,0]
                        else:
                            image[row][col] = self.images[self.current_image][int(posy-0.05*self.mywidth+row)][int(posx-0.05*self.mywidth+col)]

                img = QImage(image, image.shape[1], image.shape[0], image.shape[1] * 3,QImage.Format.Format_RGB888)
                pixmap = QPixmap(img).scaled(135, 135,Qt.AspectRatioMode.KeepAspectRatio,  Qt.TransformationMode.FastTransformation)


                s = pixmap.size()
                result =  QPixmap(s)
                result.fill(Qt.GlobalColor.transparent)
                painter = QPainter(result)
                painter.setRenderHint(QPainter.RenderHint.Antialiasing)
                painter.drawPixmap(QPoint(), pixmap)
                painter.setCompositionMode(QPainter.CompositionMode.CompositionMode_SourceOver)
                painter.drawPixmap(result.rect(), axe, axe.rect())
                painter.end()

                # size = self.pixmap.size()
                # self.pixmap.scaled(1 * size)
                self.loupeBox.setPixmap(result)
            

    # --------------------------------------------------   
    # Gestion des évènements sur le canvas mpl et
    # mises à jours correpondantes
    # --------------------------------------------------  

    def axe_clicked(self,value):
        logger.info("Clic axeButton")
        self.axis_set = True
        self.applyOrient = True

        self.old_axisParam = self.orient_update(self.axisType)

        self.axisType = value

        try:
            self.sc.mpl_disconnect(self.clickEvent)
        except:
            logger.warning("Impossible de déconnecter l'évènement de clic sur canvas. Déjà déconnecté ?!")
        logger.info("Connexion de l'évènement clic sur canvas avec la fonction axis_event")
        self.clickEvent = self.sc.mpl_connect('button_press_event',self.axis_event)

    def orient_update(self,value):
        logger.info("Mise à jour des paramètres pour l'orientation des axes")
        if value == 1:
            return (1,1)
            self.Vaxis_orient = 1
            self.Haxis_orient = 1
        elif value == 2:
            return (-1, 1)
            self.Vaxis_orient = -1
            self.Haxis_orient = 1
        elif value == 3:
            return (1,-1)
            self.Vaxis_orient = 1
            self.Haxis_orient = -1
        elif value == 4:
            return (-1,-1)
            self.Vaxis_orient = -1
            self.Haxis_orient = -1

    def axis_update(self):
        logger.info("Mise à jour des axes")
        self.sc.axes.spines["left"].set_position(("data", 0))
        self.sc.axes.spines["bottom"].set_position(("data", 0))
        self.sc.axes.spines["top"].set_visible(False)
        self.sc.axes.spines["right"].set_visible(False)

        if self.axisType == 1:
            self.sc.axes.plot(1, 0, ">k", transform=self.sc.axes.get_yaxis_transform(), clip_on=False)
            self.sc.axes.plot(0, 1, "^k", transform=self.sc.axes.get_xaxis_transform(), clip_on=False)
        elif self.axisType == 2:
            self.sc.axes.plot(1, 0, ">k", transform=self.sc.axes.get_yaxis_transform(), clip_on=False)
            self.sc.axes.plot(0, 0, "vk", transform=self.sc.axes.get_xaxis_transform(), clip_on=False)
        elif self.axisType == 3:
            self.sc.axes.plot(0, 0, "<k", transform=self.sc.axes.get_yaxis_transform(), clip_on=False)
            self.sc.axes.plot(0, 1, "^k", transform=self.sc.axes.get_xaxis_transform(), clip_on=False)
        elif self.axisType == 4:
            self.sc.axes.plot(0, 0, "<k", transform=self.sc.axes.get_yaxis_transform(), clip_on=False)
            self.sc.axes.plot(0, 0, "vk", transform=self.sc.axes.get_xaxis_transform(), clip_on=False)
    
    def repere_clicked(self):
        logger.info("Clic repereButton")
        self.etalonBox.setEnabled(True)
        changeIconTheme(self.theme, self)
        try:
            self.sc.mpl_disconnect(self.clickEvent)
        except:
            logger.warning("Impossible de déconnecter l'évènement de clic sur canvas. Déjà déconnecté ?!")
        logger.info("Connexion de l'évènement clic sur canvas avec la fonction measure_event")
        self.clickEvent = self.sc.mpl_connect('button_press_event',self.measure_event)
        self.axis_set = False

        self.repereBox.setProperty('class', 'blueBox')

        self.etalonBox.setProperty('class', 'greenBox')

        # Application du style CSS
        app.setStyleSheet(app.styleSheet())

    def axis_event(self, event):
        if self.axis_set == True and event.xdata!=None and event.ydata!=None:

            self.Haxis_ratio = (self.Haxis_ratio*self.mywidth+self.Vaxis_orient*event.ydata)/self.mywidth

            self.Vaxis_ratio = (self.Vaxis_ratio*self.myheight+self.Haxis_orient*event.xdata)/self.myheight

            # On met à jour les différents plots...
            for i in range(self.nb_images):
                if self.data_brute[1][i] != None and self.data_brute[2][i] != None:
                    
                    self.data_brute[1][i]=(self.data_brute[1][i]-event.xdata)
                    self.data_brute[2][i]=(self.data_brute[2][i]-event.ydata)

                    
                    if self.nbPoints == 2:
                        if self.data_brute[3][i] != None and self.data_brute[4][i] != None: 
                            self.data_brute[3][i]=(self.data_brute[3][i]-event.xdata)
                            self.data_brute[4][i]=(self.data_brute[4][i]-event.ydata)

                    self.table_update(i)
                        
            self.etalonnage["x1"]-=event.xdata
            self.etalonnage["x2"]-=event.xdata
            self.etalonnage["y1"]-=event.ydata
            self.etalonnage["y2"]-=event.ydata
            self.canvas_update()
            
    def etalonnage_clicked(self, value):
        try:
            self.sc.mpl_disconnect(self.clickEvent)
        except:
            logger.warning("Impossible de déconnecter l'évènement de clic sur canvas. Déjà déconnecté ?!")
        logger.info("Connexion de l'évènement clic sur canvas avec la fonction etalon_event")
        self.clickEvent = self.sc.mpl_connect('button_press_event',self.etalon_event)
        if value == 1:
            self.etalonnage["status"] = "stage1"
            #print("Prise du premier point pour l'étalonnage")
        elif value == 2:
            self.etalonnage["status"] = "stage2"
            #print("Prise du second point pour l'étalonnage")

    def ruler_clicked(self):
        logger.info("Clic rulerButton")

        self.etalonnage["valeurPixels"] = ((self.etalonnage["x1"]-self.etalonnage["x2"])**2+(self.etalonnage["y1"]-self.etalonnage["y2"])**2)**(1/2)

        if self.etalonnage["valeurPixels"] == 0:
            dlg = messageDialog("Veuillez sélectionner l'étalon sur la vidéo", self.theme)
            if dlg.exec():
                self.firstPoint.setStyleSheet("color: red;")
                self.secondPoint.setStyleSheet("color: red;")
            else :
                self.firstPoint.setStyleSheet("color: red;")
                self.secondPoint.setStyleSheet("color: red;")
        else :
            self.firstPoint.setStyleSheet("")
            self.secondPoint.setStyleSheet("")
            self.valeurEtalon.setStyleSheet("")
            if self.valeurEtalon.text() != "":
                self.buttonGroup.setExclusive(False)
                self.firstPoint.setChecked(False)
                self.secondPoint.setChecked(False)
                self.buttonGroup.setExclusive(True)

                self.validateButton.setEnabled(True)

                self.styleBox.setEnabled(True)
                self.valeurEtalon.setStyleSheet("")
                changeIconTheme(self.theme, self)
                if self.etalonnage["done"] == True:
                    self.etalonnage["old_valeurMetres"] = self.etalonnage["valeurMetres"]
                    self.etalonnage["old_valeurPixels"] = self.etalonnage["valeurPixels"]
                self.etalonnage["valeurMetres"] = float(self.valeurEtalon.text().replace(',','.'))
                
                self.etalonnage["done"] = True
                self.labelEtalon.setText(str(round(self.etalonnage["valeurPixels"],2))+" pixels équivalent à "+str(self.etalonnage["valeurMetres"])+" mètres")
                self.firstPoint.setChecked(False)
                self.secondPoint.setChecked(False)
                try:
                    self.sc.mpl_disconnect(self.clickEvent)
                except:
                    logger.warning("Impossible de déconnecter l'évènement de clic sur canvas. Déjà déconnecté ?!")
                logger.info("Connexion de l'évènement clic sur canvas avec la fonction measure_event")
                self.clickEvent = self.sc.mpl_connect('button_press_event',self.measure_event)

                self.etalonBox.setProperty('class', 'blueBox')
                self.validateButton.setProperty('class', 'greenBtn')
                self.validateButton.setIcon(qta.icon('fa5s.check-circle',color='white'))
                self.table_update_etalon()

                # Application du style CSS
                app.setStyleSheet(app.styleSheet())

                self.canvas_update()
            else :
                dlg = messageDialog("Veuillez entrer la valeur correspondante à l'étalon défini (en m) !", self.theme)
                if dlg.exec():
                    print("Success!")
                    self.valeurEtalon.setStyleSheet("border: 1px solid red;")
                else:
                    print("Cancel!")
                    self.valeurEtalon.setStyleSheet("border: 1px solid red;")

    def etalon_event(self, event):
        self.showEtalon = True
        if self.etalonnage["status"] == "stage1" and event.xdata!=None and event.ydata!=None:
            self.etalonnage["x1"] = event.xdata
            self.etalonnage["y1"] = event.ydata
        elif self.etalonnage["status"] == "stage2" and event.xdata!=None and event.ydata!=None:
            self.etalonnage["x2"] = event.xdata
            self.etalonnage["y2"] = event.ydata
        self.canvas_update()

    def settings_update(self):
        logger.info("Clic formeButton")
        colorValue = self.comboColor.currentText()
        if colorValue == "Bleu":
            self.settings["color"] = "b"
        elif colorValue == "Rouge":
            self.settings["color"] = "r"
        elif colorValue == "Vert":
            self.settings["color"] = "g"
        elif colorValue == "Cyan":
            self.settings["color"] = "c"
        elif colorValue == "Magenta":
            self.settings["color"] = "m"
        elif colorValue == "Jaune":
            self.settings["color"] = "y"
        elif colorValue == "Blanc":
            self.settings["color"] = "w"
        elif colorValue == "Noir":
            self.settings["color"] = "k"
        else:
            self.settings["color"] = "b"

        formeValue = self.comboFormat.currentText()
        if formeValue == "Point":
            self.settings["point"] = "."
        elif formeValue == "Disque":
            self.settings["point"] = "o"
        elif formeValue == "Croix":
            self.settings["point"] = "x"
        elif formeValue == "Plus":
            self.settings["point"] = "+"
        elif formeValue == "Carré":
            self.settings["point"] = "s"
        elif formeValue == "Triangle":
            self.settings["point"] = "v"
        else:
            self.settings["point"] = "o"
        
        if self.checkLine.isChecked():
            self.settings["line"] = "--"
        else:
            self.settings["line"] = ""

        if self.checkGrid.isChecked():
            self.settings["grid"] = True
        else:
            self.settings["grid"] = False

        if self.checkTicks.isChecked():
            self.settings["ticks"] = True
        else:
            self.settings["ticks"] = False

        self.canvas_update()

    def measure_event(self, event):
        if self.mesures == True and event.xdata!=None and event.ydata!=None:
            self.graphiqueButton.setEnabled(True)
            
            if self.nbPoints == 2:
                if self.sous_mesure == 0:
                    self.data_brute[0][self.current_image]=round(self.current_image*self.duration/self.nb_images,3)
                    self.data_brute[1][self.current_image]=event.xdata
                    self.data_brute[2][self.current_image]=event.ydata
                    self.table_update(self.current_image)
                    

                    self.canvas_update()
                    self.loupe_update(event)
                    self.sous_mesure = 1
                else :
                    self.data_brute[3][self.current_image]=event.xdata
                    self.data_brute[4][self.current_image]=event.ydata

                    self.table_update(self.current_image)
                    

                    self.next_clicked()
                    self.loupe_update(event)

                    self.sous_mesure = 0
            else :
                
                self.data_brute[0][self.current_image]=round(self.current_image*self.duration/self.nb_images,3)
                self.data_brute[1][self.current_image]=event.xdata
                self.data_brute[2][self.current_image]=event.ydata

                self.canvas_update()
                self.table_update(self.current_image)

                self.next_clicked()
                self.loupe_update(event)

    # --------------------------------------------------   
    # Gestion des évènements sur le tableau
    # --------------------------------------------------  

    def table_update(self,i):
        logger.info("Mise à jour de la table")
        # Ajout de la valeur de t dans la ligne correspondant à l'image
        self.item = QTableWidgetItem()
        self.item.setText(str(round(self.video_timestamp[i]/1000,3)))
        self.tableWidget.setItem(i, 0, self.item)
        self.data[0][i] = round(self.video_timestamp[i]/1000,3)

        # Ajout de la valeur de x dans la ligne correspondant à l'image
        self.data[1][i] = round(self.data_brute[1][i]*self.etalonnage["valeurMetres"]/self.etalonnage["valeurPixels"],3)
        self.item = QTableWidgetItem()
        self.item.setText(str(self.data[1][i]))
        self.tableWidget.setItem(i, 1, self.item)

        # Ajout de la valeur de y dans la ligne correspondant à l'image
        self.data[2][i] = round(self.data_brute[2][i]*self.etalonnage["valeurMetres"]/self.etalonnage["valeurPixels"],3)
        self.item = QTableWidgetItem()
        self.item.setText(str(self.data[2][i]))
        self.tableWidget.setItem(i, 2, self.item)

        if self.nbPoints == 2 and self.data_brute[3][i]!=None and self.data_brute[4][i]!=None:

            self.data[3][i] = round(self.data_brute[3][i]*self.etalonnage["valeurMetres"]/self.etalonnage["valeurPixels"],3)
            self.item = QTableWidgetItem()
            self.item.setText(str(self.data[3][i]))
            self.tableWidget.setItem(i, 3, self.item)
            
            self.data[4][i] = round(self.data_brute[4][i]*self.etalonnage["valeurMetres"]/self.etalonnage["valeurPixels"],3)
            self.item = QTableWidgetItem()
            self.item.setText(str(round(self.data[4][i]*self.etalonnage["valeurMetres"]/self.etalonnage["valeurPixels"],3)))
            self.tableWidget.setItem(i, 4, self.item)
            

    def table_update_etalon(self):
        logger.info("Mise à jour de la table sur au changement de valeur d'étalon")
        if "old_valeurMetres" in self.etalonnage:
            for row in range(self.tableWidget.rowCount()):
                if self.data_brute[1][row] is not None and self.data_brute[1][row] is not None:
                    # x_value = round(float(x_item.text())*self.etalonnage["valeurMetres"]*self.etalonnage["old_valeurPixels"]/(self.etalonnage["valeurPixels"]*self.etalonnage["old_valeurMetres"]),3)
                    # y_value = round(float(y_item.text())*self.etalonnage["valeurMetres"]*self.etalonnage["old_valeurPixels"]/(self.etalonnage["valeurPixels"]*self.etalonnage["old_valeurMetres"]),3)

                    self.data[1][row] = round(self.data_brute[1][row]*self.etalonnage["valeurMetres"]/self.etalonnage["valeurPixels"],3)
                    self.data[2][row] = round(self.data_brute[2][row]*self.etalonnage["valeurMetres"]/self.etalonnage["valeurPixels"],3)

                    self.tableWidget.item(row, 1).setText(str(self.data[1][row]))
                    self.tableWidget.item(row, 2).setText(str(self.data[2][row]))

                if self.nbPoints == 2:
                    if self.data[3][row] is not None and self.data[4][row] is not None:
                        self.data[3][row] = round(self.data_brute[3][row]*self.etalonnage["valeurMetres"]/self.etalonnage["valeurPixels"],3)
                        self.data[4][row] = round(self.data_brute[4][row]*self.etalonnage["valeurMetres"]/self.etalonnage["valeurPixels"],3)

                        self.tableWidget.item(row, 1).setText(str(self.data[1][row]))
                        self.tableWidget.item(row, 2).setText(str(self.data[2][row]))



    def table_clicked(self,item):
        logger.info("Valeur de la table modifiée par l'utilisateur")
        self.tableWidget.itemChanged.connect(lambda : self.table_changed(item))

    def table_changed(self,item):
        if item.column() == 1:
            if item.text() != '':
                self.data_brute[1][item.row()] = float(item.text())*self.etalonnage["valeurPixels"]/self.etalonnage["valeurMetres"]
            else:
                self.data_brute[1][item.row()] = None
        elif item.column() == 2:
            if item.text() != '':
                self.data_brute[2][item.row()] = float(item.text())*self.etalonnage["valeurPixels"]/self.etalonnage["valeurMetres"]
            else:
                self.data_brute[2][item.row()] = None
        if item.column() == 3:
            if item.text() != '':
                self.data_brute[3][item.row()] = float(item.text())*self.etalonnage["valeurPixels"]/self.etalonnage["valeurMetres"]
            else:
                self.data_brute[1][item.row()] = None
        elif item.column() == 4:
            if item.text() != '':
                self.data_brute[4][item.row()] = float(item.text())*self.etalonnage["valeurPixels"]/self.etalonnage["valeurMetres"]
            else:
                self.data_brute[2][item.row()] = None
        try: self.tableWidget.itemChanged.disconnect() 
        except Exception as ex: 
            logger.warning("Impossible de déconnecter l'évènement itemChanged sur tableWidget. Déjà déconnecté ?!")
        self.canvas_update()

    def row_changed(self,item):
        self.current_image = item.row()
        self.horizontalSlider.setValue(self.current_image+1)
        self.canvas_update()

    def row_changed_header(self,item):
        self.current_image = item
        self.horizontalSlider.setValue(self.current_image+1)
        self.canvas_update()


    # --------------------------------------------------   
    # Fonction principale
    # --------------------------------------------------  

    def canvas_update(self):
        logger.info("Mise à jour du canvas")
        if self.etalonnage["done"] == True:
            old_labels = self.sc.axes.get_xticklabels()

        self.imageLabel.setText('Image : '+str(self.current_image+1)+'/'+str(self.nb_images))
        self.tableWidget.selectRow(self.current_image)
        self.sc.axes.cla() 

        if self.axisType == 1:
            self.Vaxis_orient = 1
            self.Haxis_orient = 1
        elif self.axisType == 2:
            self.Vaxis_orient = -1
            self.Haxis_orient = 1
        elif self.axisType == 3:
            self.Vaxis_orient = 1
            self.Haxis_orient = -1
        elif self.axisType == 4:
            self.Vaxis_orient = -1
            self.Haxis_orient = -1

        
        bottom = -self.mywidth*self.Haxis_ratio
        top = self.mywidth-abs(bottom)
        left = -self.myheight*self.Vaxis_ratio
        right = self.myheight-abs(left)
        self.myextent=[self.Haxis_orient*left, self.Haxis_orient*right,self.Vaxis_orient*bottom, self.Vaxis_orient*top]
        self.ratio = [left,right,bottom,top]

        #self.myextent=[-self.ycoef*self.myheight*self.ycenter, self.ycoef*self.myheight*(1-self.ycenter),-self.xcoef*self.mywidth*self.xcenter, self.xcoef*self.mywidth*(1-self.xcenter)]
        
        self.sc.axes.imshow(self.images[self.current_image], extent=self.myextent)

        if self.etalonnage["x1"] != 0 or self.etalonnage["x2"] != 0 or self.etalonnage["y1"] != 0 or self.etalonnage["y2"] != 0:
            if self.applyOrient == True:

                self.etalonnage["x1"]*=self.Haxis_orient*self.old_axisParam[1]
                self.etalonnage["x2"]*=self.Haxis_orient*self.old_axisParam[1]
                self.etalonnage["y1"]*=self.Vaxis_orient*self.old_axisParam[0]
                self.etalonnage["y2"]*=self.Vaxis_orient*self.old_axisParam[0]

                
            if self.showEtalon == True:
                self.sc.axes.plot([self.etalonnage["x1"],self.etalonnage["x2"]],[self.etalonnage["y1"],self.etalonnage["y2"]], "ro-")

        for k in range(self.nb_images):
            if self.data_brute[1][k] != None or self.data_brute[2][k] != None:
                if self.applyOrient == True:
                    if self.nbPoints == 2 :
                        if self.data_brute[3][k] != None or self.data_brute[4][k] != None:
                            self.data_brute[1][k]*=self.Haxis_orient*self.old_axisParam[1]
                            self.data_brute[2][k]*=self.Vaxis_orient*self.old_axisParam[0]
                            self.data_brute[3][k]*=self.Haxis_orient*self.old_axisParam[1]
                            self.data_brute[4][k]*=self.Vaxis_orient*self.old_axisParam[0]
                            self.table_update(k)
                    else:
                        self.data_brute[1][k]*=self.Haxis_orient*self.old_axisParam[1]
                        self.data_brute[2][k]*=self.Vaxis_orient*self.old_axisParam[0]
                        self.table_update(k)

        
        if self.etalonnage["done"] == True:
            # labels = [round(float(item.get_text().replace('−','-'))*self.etalonnage["valeurMetres"]/self.etalonnage["valeurPixels"],3)  for item in old_labels]
            list = linspace(-max(abs(left),abs(right)), max(abs(left),abs(right)), 6)
            self.sc.axes.set_xticks(list)
            labels = [round(i*self.etalonnage["valeurMetres"]/self.etalonnage["valeurPixels"],3)  for i in list]
            
            self.sc.axes.set_xticklabels(labels)

            list = linspace(-max(abs(bottom),abs(top)), max(abs(bottom),abs(top)), 6)
            self.sc.axes.set_yticks(list)
            labels = [round(i*self.etalonnage["valeurMetres"]/self.etalonnage["valeurPixels"],3)  for i in list]
            self.sc.axes.set_yticklabels(labels)

        self.sc.axes.set_xlim([self.Haxis_orient*left, self.Haxis_orient*right])
        self.sc.axes.set_ylim([self.Vaxis_orient*bottom, self.Vaxis_orient*top])
        self.applyOrient = False
            
        if self.nbPoints == 2:
            self.sc.axes.plot(self.data_brute[1],self.data_brute[2],str(self.settings["color"]+self.settings["point"]+self.settings["line"]))
            self.sc.axes.plot(self.data_brute[3],self.data_brute[4],str(self.settings["point"]+self.settings["line"]), color="tab:pink")

            for i in range(self.nb_images):
                if self.data_brute[1][i] != None or self.data_brute[2][i] != None:
                    self.sc.axes.annotate("1", 
                                (self.data_brute[1][i],self.data_brute[2][i]), 
                                color=self.settings["color"])

            for i in range(self.nb_images):
                if self.data_brute[3][i] != None or self.data_brute[4][i] != None:
                    self.sc.axes.annotate("2", 
                                (self.data_brute[3][i],self.data_brute[4][i]), 
                                color="tab:pink")
        else:
            self.sc.axes.plot(self.data_brute[1],self.data_brute[2],str(self.settings["color"]+self.settings["point"]+self.settings["line"]))
    
        self.sc.axes.grid(self.settings["grid"])
        if self.settings["ticks"] == False :
            self.sc.axes.set_xticklabels([])
            self.sc.axes.set_yticklabels([])

        self.sc.draw_idle()

        self.axis_update()

    # --------------------------------------------------   
    # Mise à jour de l'interface et des évènements
    # --------------------------------------------------  

    def start_measures(self):
        logger.info("Démarrage des mesures")
        if self.etalonnage["done"] == True:
            self.tabWidget.setTabEnabled(1, True)
            self.tabWidget.setCurrentIndex(1)

            # Récupérer la valeur contenue dans la combobox comboNbPoints
            self.nbPoints = int(self.comboNbPoints.currentText())
            if self.nbPoints ==2:
                self.tableWidget.setColumnCount(5)
                self.tableWidget.setHorizontalHeaderLabels(["Temps (s)", "x1 (m)", "y1 (m)", "x2 (m)", "y2 (m)"])
            else :
                self.tableWidget.setColumnCount(3)
                self.tableWidget.setHorizontalHeaderLabels(["Temps (s)", "x (m)", "y (m)"])

            self.tableWidget.selectRow(self.current_image)
            self.repere_clicked()
            self.ruler_clicked()
            try:
                self.sc.mpl_disconnect(self.clickEvent)
            except:
                logger.warning("Impossible de déconnecter l'évènement de clic sur canvas. Déjà déconnecté ?!") 
            logger.info("Connexion de l'évènement clic sur canvas avec la fonction measure_event")
            self.clickEvent = self.sc.mpl_connect('button_press_event',self.measure_event)
            self.mesures = True
        else:
            dlg = messageDialog("Veuillez réaliser l'étalonnage avant de commencer les mesures !", self.theme)
            if dlg.exec():
                self.repere_clicked()
                print("Success!")
            else:
                self.repere_clicked()
                print("Cancel!")

    def tabbar_clicked(self, index):
        logger.info("Clic sur tab : " + str(index))
        if index == 0:
            self.mesures = False
            try:
                self.sc.mpl_disconnect(self.clickEvent)
            except:
                logger.warning("Impossible de déconnecter l'évènement de clic sur canvas. Déjà déconnecté ?!") 
            
        elif index == 1:
            self.mesures = True
            self.start_measures()
        

    def play_update(self,value,axes):
        self.current_image = value
        self.horizontalSlider.setValue(self.current_image+1)
        self.imageLabel.setText('Image : '+str(self.current_image+1)+'/'+str(self.nb_images))
        self.sc.axes=axes
        self.sc.draw_idle()
        self.axis_update()

    def slider_update(self,value):
        if self.playStatus == False:
            self.current_image = value-1
            self.canvas_update()

    def icon_from_svg(self,svg_filepath):
        img = QPixmap(svg_filepath)
        qp = QPainter(img)
        qp.setCompositionMode(QPainter.CompositionMode.CompositionMode_SourceIn)
        qp.fillRect( img.rect(), Qt.GlobalColor.white )
        qp.end()
        return QIcon(img)

    def save_clicked(self,value):
        logger.info("Clic sur saveButton avec option : " + str(value))
        if value == 0:
            if self.nbPoints == 2:
                clipboard = 'Temps (s)\tx1 (m)\ty1(m)\tx2 (m)\ty2(m)\r\n'
                for row in range(self.tableWidget.rowCount()):
                    rowdata = []
                    for column in range(self.tableWidget.columnCount()):
                        item = self.tableWidget.item(row, column)
                        if item is not None:
                            mytext = item.text()
                            mytext = mytext.replace('.',',',1)
                            rowdata.append(mytext)
                        else:
                            rowdata.append('')
                    if rowdata != ['','','','','']:
                        clipboard+=str(rowdata[0])+'\t'+str(rowdata[1])+'\t'+str(rowdata[2])+'\t'+str(rowdata[3])+'\t'+str(rowdata[4])+'\r\n'
            else:
                clipboard = 'Temps (s)\tx (m)\ty(m)\r\n'
                for row in range(self.tableWidget.rowCount()):
                    rowdata = []
                    for column in range(self.tableWidget.columnCount()):
                        item = self.tableWidget.item(row, column)
                        if item is not None:
                            mytext = item.text()
                            mytext = mytext.replace('.',',',1)
                            rowdata.append(mytext)
                        else:
                            rowdata.append('')
                    if rowdata != ['','','']:
                        clipboard+=str(rowdata[0])+'\t'+str(rowdata[1])+'\t'+str(rowdata[2])+'\r\n'
            try :
                pccopy(clipboard)
                logger.info("Copie dans le presse-papier réussie !")
                self.show_toast("Succès !", "Les données ont été copiées dans le presse-papier !")
            except Exception as ex :
                logger.warning("Une erreur est survenue : " + ex)
        elif value == 1:
            clipboard = 'liste_t = ['
            for val in self.data[0]:
                if val != None:
                    clipboard+=str(val)+','
            clipboard = clipboard[:-1]
            clipboard += ']\r\n'
            clipboard += 'liste_x = ['
            for val in self.data[1]:
                if val != None:
                    clipboard+=str(val)+','
            clipboard = clipboard[:-1]
            clipboard += ']\r\n'
            clipboard += 'liste_y = ['
            for val in self.data[2]:
                if val != None:
                    clipboard+=str(val)+','
            clipboard = clipboard[:-1]
            clipboard += ']\r\n'

            if self.nbPoints == 2:
                clipboard += 'liste_x2 = ['
                for val in self.data[3]:
                    if val != None:
                        clipboard+=str(val)+','
                clipboard = clipboard[:-1]
                clipboard += ']\r\n'
                clipboard += 'liste_y2 = ['
                for val in self.data[4]:
                    if val != None:
                        clipboard+=str(val)+','
                clipboard = clipboard[:-1]
                clipboard += ']\r\n'

            try :
                pccopy(clipboard)
                logger.info("Copie dans le presse-papier réussie !")
                self.show_toast("Succès !", "Les données ont été copiées dans le presse-papier !")
            except Exception as ex :
                logger.warning("Une erreur est survenue : " + ex)
        elif value == 2:
            path, ok = QFileDialog.getSaveFileName(
                None, 'Sauvegarder les données', os.getenv('HOME'), 'Fichier CSV (*.csv)')
            if ok:
                columns = range(self.tableWidget.columnCount())
                header = [self.tableWidget.horizontalHeaderItem(column).text()
                        for column in columns]
                suffix = ".csv"
                if ".csv" in path:
                    suffix = ""
                try :
                    with open(path+suffix, 'w') as csvfile:
                        writer = csv.writer(
                            csvfile, dialect='excel',  delimiter=';', lineterminator='\n')
                        writer.writerow(header)
                        for row in range(self.tableWidget.rowCount()):
                            rowdata = []
                            for column in range(self.tableWidget.columnCount()):                        
                                item = self.tableWidget.item(row, column)
                                if item is not None:
                                    mytext = item.text()
                                    mytext = mytext.replace('.',',',1)
                                    rowdata.append(mytext)
                                else:
                                    rowdata.append('')
                            if rowdata != ['','','']:
                                writer.writerow(rowdata)
                    self.show_toast("Succès !", "Les données ont été sauvegardées dans le fichier CSV !")
                    logger.info("Écriture du fichier csv terminée avec succès")
                except Exception as ex:
                    logger.warning("Une erreur est survenue : " + ex)
        elif value == 3:
            path, ok = QFileDialog.getSaveFileName(
                None, 'Sauvegarder les données', os.getenv('HOME'), 'Script Python (*.py)')
            if ok:
                newt,newx,newy,newx2,newy2=[],[],[],[],[]
                for row in range(self.tableWidget.rowCount()):
                    try :
                        item = self.tableWidget.item(row, 0).text()
                        newt.append(float(item))
                        item = self.tableWidget.item(row, 1).text()
                        newx.append(float(item))
                        item = self.tableWidget.item(row, 2).text()
                        newy.append(float(item))
                        if self.nbPoints == 2:
                            item = self.tableWidget.item(row, 3).text()
                            newx2.append(float(item))
                            item = self.tableWidget.item(row, 4).text()
                            newy2.append(float(item))
                    except :
                        pass

                if self.etalonnage["done"] == True:
                    left = self.ratio[0]*self.etalonnage["valeurMetres"]/self.etalonnage["valeurPixels"]
                    right = self.ratio[1]*self.etalonnage["valeurMetres"]/self.etalonnage["valeurPixels"]
                    bottom = self.ratio[2]*self.etalonnage["valeurMetres"]/self.etalonnage["valeurPixels"]
                    top = self.ratio[3]*self.etalonnage["valeurMetres"]/self.etalonnage["valeurPixels"]

                if self.nbPoints == 2:
                    filecontent ='import matplotlib.pyplot as plt\r\n\r\nt='+str(newt)+'\r\nx='+str(newx)+'\r\ny='+str(newy)+'\r\nx2='+str(newx2)+'\r\ny2='+str(newy2)+'\r\n\r\nplt.xlim(['+str(left)+','+str(right)+'])\r\nplt.ylim(['+str(bottom)+','+str(top)+'])\r\nplt.plot(x,y,\'bo\')\r\nplt.plot(x2,y2,\'ro\')\r\nplt.xlabel("x (m)")\r\nplt.ylabel("y (m)")\r\nplt.grid()\r\nplt.show()'
                else :
                    filecontent ='import matplotlib.pyplot as plt\r\n\r\nt='+str(newt)+'\r\nx='+str(newx)+'\r\ny='+str(newy)+'\r\n\r\nplt.xlim(['+str(left)+','+str(right)+'])\r\nplt.ylim(['+str(bottom)+','+str(top)+'])\r\nplt.plot(x,y,\'bo\')\r\nplt.xlabel("x (m)")\r\nplt.ylabel("y (m)")\r\nplt.grid()\r\nplt.show()'
                suffix = ".py"
                if ".py" in path:
                    suffix = ""
                try :
                    with open(path+suffix, 'w') as pyfile:
                        pyfile.write(filecontent)
                    self.show_toast("Succès !", "Les données ont été sauvegardées dans le fichier Python !")
                    logger.info("Écriture du fichier py terminée avec succès")
                except Exception as ex:
                    logger.warning("Une erreur est survenue : " + ex)
        elif value == 4:
            filePath, _ = QFileDialog.getSaveFileName(self, "Image", "",
                            "PNG(*.png);;JPEG(*.jpg *.jpeg);;All Files(*.*) ")
            if filePath == "":
                return
            
            try :
                # Sauvegarde du canvas
                self.sc.print_figure(filePath)
                self.show_toast("Succès !", "L'image a été sauvegardée avec succès !")
                logger.info("Écriture du fichier png terminée avec succès")
            except Exception as ex:
                logger.warning("Une erreur est survenue : " + ex)

    def infos_clicked(self):
        dlg = messageDialog(self.version, self.theme)
        if dlg.exec():
            print("Success!")
        else:
            print("Cancel")

    def tutoriel_clicked(self):
        webbrowser.open('https://www.ensciences.fr/read.php?article=820#interface')

    def bug_clicked(self):
        webbrowser.open('https://www.ensciences.fr/contact.php?objet=chronophys')

    def menu_clicked(self):
        if self.menuBar().isHidden():
            self.menuBar().show()
        else:
            self.menuBar().hide()

    def theme_clicked(self, value):
        logger.info("Clic sur themeButton avec option : " + str(value))
        if value == "dark":
            self.theme = "dark"
            app.setStyleSheet(getCSS("dark"))
            changeIconTheme("dark", self)
        else :
            self.theme = "light"
            app.setStyleSheet(getCSS("light"))
            changeIconTheme("light", self)
        # Application du style CSS
        app.setStyleSheet(app.styleSheet())

    # Shows a toast notification every time the button is clicked
    def show_toast(self, title, content):
        # Afficher une notification
        self.tray_icon.showMessage(title, content)

        
        
    # --------------------------------------------------   
    # Gestion de la fermeture
    # -------------------------------------------------- 

    def closeEvent(self, event):
        dlg = messageDialog("Voulez-vous vraiment quitter ?", self.theme)
        if dlg.exec():
            event.accept()
        else:
            event.ignore()

# --------------------------------------------------   
# Classe Worker exécutant la lecture dans un thread
# -------------------------------------------------- 

class Worker(QObject):
    finished = pyqtSignal()
    data = pyqtSignal(int,object)

    def __init__(self, images, data_brute, nbPoints, axes,myextent,etalonnage,showEtalon,ratio,settings,current_image, nb_images, parent=None):
        super(Worker, self).__init__(parent)
        logger.info("Démarrage du Worker pour la lecture")

        self.axes = axes
        self.current_number = current_image
        self.nb_images = nb_images
        self.images = images
        self.data_brute = data_brute
        self.nbPoints = nbPoints
        self.myextent = myextent
        self.etalonnage = etalonnage
        self.left = ratio[0]
        self.right = ratio[1]
        self.bottom = ratio[2]
        self.top = ratio[3]
        self.settings=settings
        self.showEtalon = showEtalon

        self.threadactive=True

    def run(self):

        k = self.current_number
        for i in range(k,self.nb_images):
            self.axes.cla() 
            self.axes.imshow(self.images[i], extent=self.myextent)
            
            if self.nbPoints == 2:
                self.axes.plot(self.data_brute[1],self.data_brute[2],str(self.settings["color"]+self.settings["point"]+self.settings["line"]))
                self.axes.plot(self.data_brute[3],self.data_brute[4],str(self.settings["point"]+self.settings["line"]), color="tab:pink")

                for i in range(self.nb_images):
                    if self.data_brute[1][i] != None or self.data_brute[2][i] != None:

                        self.axes.annotate("1", 
                                    (self.data_brute[1][i],self.data_brute[2][i]), 
                                    color=self.settings["color"])

                for i in range(self.nb_images):
                    if self.data_brute[3][i] != None or self.data_brute[4][i] != None:

                        self.axes.annotate("2", 
                                    (self.data_brute[3][i],self.data_brute[4][i]), 
                                    color="tab:pink")
            else:
                self.axes.plot(self.data_brute[1],self.data_brute[2],str(self.settings["color"]+self.settings["point"]+self.settings["line"]))


            if self.showEtalon == True:
                self.axes.plot([self.etalonnage["x1"],self.etalonnage["x2"]],[self.etalonnage["y1"],self.etalonnage["y2"]], "ro-")

            if self.etalonnage["done"] == True:
                list = linspace(-max(abs(self.left),abs(self.right)), max(abs(self.left),abs(self.right)), 6)
                self.axes.set_xticks(list)
                labels = [round(i*self.etalonnage["valeurMetres"]/self.etalonnage["valeurPixels"],3)  for i in list]
                self.axes.set_xticklabels(labels)

                list = linspace(-max(abs(self.bottom),abs(self.top)), max(abs(self.bottom),abs(self.top)), 6)
                self.axes.set_yticks(list)
                labels = [round(i*self.etalonnage["valeurMetres"]/self.etalonnage["valeurPixels"],3)  for i in list]

                self.axes.grid(self.settings["grid"])
                self.axes.get_xaxis().set_visible(self.settings["ticks"])
                self.axes.get_yaxis().set_visible(self.settings["ticks"])

                self.axes.set_yticklabels(labels)

            self.data.emit(i,self.axes)
            sleep(0.3)
            if self.threadactive != True:
                break
        self.threadactive = False
        self.finished.emit()

    def stop(self):
        self.threadactive = False
        self.finished.emit()
        logger.info("Arrêt Worker pour la lecture")

# --------------------------------------------------   
# Execution de l'application
# -------------------------------------------------- 

if __name__ == "__main__":
    logger.info("-------------------------------------------------------------")
    logger.info("Démarrage de l'application Chronophys")
    logger.info("Version de l'application : "+ version_number)
    logger.info("Plateforme : "+str(sys.platform))
    logger.info("-------------------------------------------------------------")
    try:
        logger.warning("Création du dossier videos")
        os.mkdir("./videos")
    except FileExistsError:
        logger.warning("Le dossier videos existe déjà")

    app = QApplication(sys.argv)
    
    ui = Window()
    sys.exit(app.exec())