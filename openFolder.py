import sys
import os
from logger import logger
import subprocess

def openFolder(application_path):
    logger.info("Ouverture du dossier contenant les vidéos")
    if sys.platform == "win32":
        opener = "explorer"
    elif sys.platform == "darwin":
        opener = "open" 
    else :
        opener = "xdg-open"
    try :
        subprocess.call([opener, os.path.join(application_path ,  "videos", "")])
    except Exception as ex:
        logger.exception("Une erreur est survenue : " + str(ex))