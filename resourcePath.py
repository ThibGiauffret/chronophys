import sys
import os

# --------------------------------------------------   
# Gestion du chemin des ressources pour les executables
# -------------------------------------------------- 
    
def resourcePath(relative_path):
    if hasattr(sys, '_MEIPASS'):
        return os.path.join(sys._MEIPASS, relative_path)
    return os.path.join(os.path.abspath("."), relative_path)