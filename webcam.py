# --------------------------------------------------
# webcam.py
# This file contains the functions to manage the webcam direct rendering and the webcam parameters.
# Capture functions are in the webcamWorker.py file.
# --------------------------------------------------

from cv2 import (
    VideoCapture,
    CAP_PROP_FRAME_WIDTH,
    CAP_PROP_FRAME_HEIGHT,
    CAP_PROP_FPS,
    flip,
    CAP_PROP_EXPOSURE,
    CAP_PROP_AUTO_EXPOSURE,
    CAP_DSHOW,
    error
)
import sys

from numpy import log as ln

# Start the webcam
def webcam_init(camera_id, params=None):
    
    # If the platform is Windows, use CAP_DSHOW to avoid some issues with some cameras
    if sys.platform == "win32":
        cap = VideoCapture(camera_id, CAP_DSHOW)
    else:
        cap = VideoCapture(camera_id)

    # Get the saved parameters if they exist, otherwise set the default parameters
    if params!=None:
        fps = params["fps"]
        res_height = params["res_height"]
        res_width = params["res_width"]
        auto_exposition = params["auto_exposition"]
        exposition = params["exposition"]
        exposition_win = params["exposition_win"]
        luminosite = params["luminosite"]
        contraste = params["contraste"]
        saturation = params["saturation"]
        gamma = params["gamma"]
        nettete = params["nettete"]
        blanc = params["blanc"]
        teinte = params["teinte"]
    else:   
        params = {}
        fps = 30
        res_height = cap.get(CAP_PROP_FRAME_HEIGHT)
        res_width = cap.get(CAP_PROP_FRAME_WIDTH)
        auto_exposition = 3 # 3 : auto mode, 1 : manual mode
        exposition_win = -6
        if sys.platform == "win32":
            exposition = 2**exposition_win*1000
        else :
            exposition = 20

        luminosite = 100
        contraste = 30
        saturation = 100
        gamma = 0
        nettete = 128
        blanc = 5500
        teinte = 0

        params["fps"],params["res_width"],params["res_height"],params["auto_exposition"],params["exposition"],params["exposition_win"], params["luminosite"], params["contraste"], params["saturation"], params["gamma"], params["nettete"], params["blanc"], params["teinte"] = int(fps), int(res_width), int(res_height),int(auto_exposition),int(exposition),int(exposition_win), int(luminosite), int(contraste), int(saturation), int(gamma), int(nettete), int(blanc), int(teinte)

    # Apply the parameters
    cap.set(CAP_PROP_FPS, fps)
    cap.set(CAP_PROP_FRAME_HEIGHT, res_height)
    cap.set(CAP_PROP_FRAME_WIDTH, res_width)
    cap.set(CAP_PROP_AUTO_EXPOSURE, auto_exposition)

    if sys.platform == "win32":
        cap.set(CAP_PROP_EXPOSURE, exposition_win)
    else:
        cap.set(CAP_PROP_EXPOSURE, exposition)
    cap.set(10,luminosite)
    cap.set(11, contraste)
    cap.set(12,saturation)
    cap.set(22,gamma)
    cap.set(20,nettete)
    cap.set(44,0) # Disable automatic white balance
    cap.set(45,blanc)
    cap.set(13, teinte)
    
    return cap, params
    
# Set the webcam parameters when the user changes them in webcamDialog
def set_property(property, value, cap):
    if property == "luminosite":
        cap.set(10, value)
    elif property == "contraste":
        cap.set(11, value)
    elif property == "saturation":
        cap.set(12, value)
    elif property == "gamma":
        cap.set(22, value)
    elif property == "nettete":
        cap.set(20, value)
    elif property == "blanc":
        cap.set(45, value)
    elif property == "teinte":
        cap.set(13, value)
    return cap

# Set the exposition of the webcam when the user changes it in webcamDialog
def set_exposition(value, check, cap):
    if not check:
        cap.set(CAP_PROP_AUTO_EXPOSURE, 1) # mode manuel
        if sys.platform == "win32":
            cap.set(CAP_PROP_EXPOSURE, int(ln(value*0.001)/ln(2)))
        else:
            cap.set(CAP_PROP_EXPOSURE, value) 
    else:
        cap.set(CAP_PROP_AUTO_EXPOSURE, 3) # mode auto
    return cap

# Get the image from the webcam
def webcam_get_image(cap):
    ret, image = cap.read()
    if ret != False:
        image = flip(image, 1)
    return ret, image

# Release the webcam
def release_cap(cap):
    if cap != None:
        cap.release()

# List the available webcam ports
def list_webcam_ports():
    non_working_ports = []
    dev_port = 0
    working_ports = []
    available_ports = []
    # Check all ports until 3 non working ports are found
    while len(non_working_ports) < 3:
        try:
            # Using DSHOW because of some issues on windows loading logitech c270... Set a fallback to CAP_ANY if it does not work ?
            if sys.platform == "win32":
                camera = VideoCapture(dev_port, CAP_DSHOW)
            else:
                camera = VideoCapture(dev_port)
            if not camera.isOpened():
                # print(f"Cannot open camera at port {dev_port}")
                non_working_ports.append(dev_port)
                camera.release()
            else:
                # print("Port %s is working." %dev_port)
                is_reading, img = camera.read()
                w = camera.get(3)
                h = camera.get(4)
                if is_reading:
                    # print("Port %s is working and reads images (%s x %s)" %(dev_port,h,w))
                    working_ports.append(dev_port)
                else:
                    # print("Port %s for camera ( %s x %s) is present but does not reads." %(dev_port,h,w))
                    available_ports.append(dev_port)
                camera.release()
        except error as e:
            print(f"An error occurred: {e}")
        dev_port +=1
    return working_ports