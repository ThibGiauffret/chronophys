from PyQt6.QtCore import QObject, pyqtSignal
from logger import logger
from webserver import webserver

# --------------------------------------------------   
# Classe WebWorker exécutant le serveur web dans un thread
# -------------------------------------------------- 

class webWorker(QObject):
    finished = pyqtSignal()
    video = pyqtSignal(str)

    def __init__(self, parent=None):
        super(webWorker, self).__init__(parent)
        logger.info("Démarrage du Worker pour le serveur web")
        self.threadactive=True
        self.server = webserver

    def run(self):
        try:
            self.server.start_server(self)
        except Exception as ex:
            logger.exception("Une erreur est survenue : " + str(ex))
        self.threadactive = False
        self.finished.emit()

    def stop(self):
        self.threadactive = False
        self.finished.emit()
        logger.info("Arrêt du Worker pour le serveur web")